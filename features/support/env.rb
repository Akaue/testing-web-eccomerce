require "capybara"
require "capybara/cucumber"
require "selenium-webdriver"


Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app,:browser => :chrome,timeout: 30)
end


Capybara.configure do |config|
  config.default_driver =   :selenium_chrome_headless
  config.app_host =  "http://automationpractice.com"
  config.default_max_wait_time = 10
end

ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)
require 'bundler/setup' # Set up gems listed in the Gemfile.
Bundler.require(:default)